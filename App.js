import React from 'react';
import { NativeRouter, Route, Switch } from 'react-router-native';

import Home from './components/Home.jsx'
import List from './components/List.jsx'
import Details from './components/Details.jsx'
export default function App() {
  return (
    <NativeRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/list" component={List} />
        <Route path="/book/:id" component={Details} />
      </Switch>
    </NativeRouter>

  );
}
