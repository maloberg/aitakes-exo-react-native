import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Slider from './Slider.jsx'
import Navigation from './Navigation.jsx'
export default function Home() {
	return (
		<View>
			<Slider/>

				<Text style={styles.homeTitle}>
					Bonjour c'est bob mais sur mobile				
				</Text>
				<Text style={styles.homeContent}>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi dolor dicta debitis eum
					similique perferendis corrupti totam nisi quia at! Dolor quaerat quidem neque laboriosam delectus ut in ab modi.
				</Text>
			<Navigation/>
		</View>
	)
}

const styles = StyleSheet.create({
	container:{
		flex:1
	},
	homeTitle: {
		fontSize: 40,
		marginTop: 10,
		textAlign: 'center'
	},
	homeContent: {
		fontSize: 14,
		padding: 25,
		textAlign: 'center',

	}})
