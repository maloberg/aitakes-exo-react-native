import React from 'react'
import { StyleSheet, Text, View, Pressable } from 'react-native'
import {useHistory} from 'react-router-native'
const Navigation = () => {
	const history = useHistory();
	return (
		<View style={styles.navigation}>
			<Pressable style={styles.buttons} onPress={() => history.push('/')}>
				<Text style={styles.buttonsText}>Accueil</Text>
			</Pressable>
			<Pressable style={styles.buttons} onPress={() => history.push('/list')}>
				<Text style={styles.buttonsText}>Liste</Text>
			</Pressable>
		</View>

	)
}
const styles = StyleSheet.create({
	navigation: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	buttons: {
		alignItems: 'center',
		backgroundColor: 'blue',
		borderRadius: 3,
		elevation: 3,
		flex: 1,
		justifyContent: 'center',
		marginBottom: 5,
		marginHorizontal: 15,
		paddingHorizontal: 30,
		paddingVertical: 15
	},
	buttonsText: {
		color: 'white',
		fontSize: 20,
		textTransform: 'capitalize'
	}
});


export default Navigation
