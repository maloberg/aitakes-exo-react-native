import React,{useRef} from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar,Animated,Dimensions, Image } from 'react-native';
import img1 from '../img/img1.jpeg';
import img2 from '../img/img2.jpeg';
import img3 from '../img/img3.jpeg';

const {width, height} =Dimensions.get('window');
const DATA = [
  {
    id: '1',
    img: img1,
  },
  {
    id: '2',
    img: img2,
  },
  {
    id: '3',
    img: img3,
  },
];

const Item = ({ img }) => (

    <Image source={img} style={styles.item}/>
);

const Slider = () => {
	const scrollX = useRef(new Animated.Value(0)).current;
  const renderItem = ({ item }) => (
    <Item img={item.img} />
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={DATA}
        horizontal
        pagingEnabled
		showsHorizontalScrollIndicator={false}
        keyExtractor={item => item.id}
        renderItem={renderItem}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: StatusBar.currentHeight || 0,
	borderBottomColor: 'red',
	borderBottomWidth:	3
  },
  item: {
    height: height /3,
    width: width
  },
  title: {
    fontSize: 32,
  },
});

export default Slider;