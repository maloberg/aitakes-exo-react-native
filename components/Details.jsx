import React from 'react'
import { StyleSheet, Text, View, Button, Dimensions } from 'react-native'
const {width, height} =Dimensions.get('window');
const Details = ({ location:
		{state:
			{img, title, description}
		}}) => 
{
	return (
		<View>
			<Image style={styles.img} source={{uri:img}} alt="bob" />
			<Text>{title}</Text>
			<Text>{description}</Text>
			<Button title="retour" onPress={()=>
			{
				<Link to={'/list'}/>
			}} />
		</View>
	)
}

export default Details


const styles = StyleSheet.create({
	img:{
		width: width/4,
		height:height/8
	}
})
