import React, {useState} from 'react'
import Navigation from './Navigation'
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar,Animated,Dimensions, Image, TextInput } from 'react-native';
import { Link } from 'react-router-native';
import useFetch from '../library/useFetch'


const {width, height} =Dimensions.get('window');

const List = () => {
const [value, setValue] = useState('')
	// Pattern pour chaque "card"
	const ListeCard = ({title, description, imgurl,id}) =>
	{
		return(
			<View>
				<Link
					to=
					{{
						pathname:`/books/${id}`,
						state: {
						img: imgurl,
						title: title,
						description:description
					}}}
				>
				<Image style={styles.img} source={{uri:imgurl}} alt="bob" />
				</Link>
			</View>
		)
	}

	
	// Utilisation d'un hook personalise pour interagir avec l'api
	const [loading, items] = 
	useFetch('https://api.nytimes.com/svc/books/v3/lists/current/hardcover-fiction.json?api-key=TA0YR2IIJVhseTJWYgGebidcA24WuNgH')	
	const books = [];

	// Verification du chargement des intructions de l'api
	if(!loading)
	{
		items.results.books.map(element =>
		{
			//Systeme de tri lie a la barre de recherche 
			if(element.title.indexOf(value.toUpperCase()) == -1)
			{
				return
			}
			// Creation du tableau d'affichage
			books.push(<ListeCard key={element.rank} title={element.title} 
				description={element.description} imgurl={element.book_image} id={element.id}/>)							
		})
	}

	return (
		<View>
			<View >
			<TextInput
				style={{height: 40}}
				placeholder="Rechercher..."
				defaultValue={value}
				onChangeText={ value => setValue(value)}
			/>
			</View>
			
			{loading && <Text>..chargement</Text>}
			{books}
			
			<Navigation/>
		</View>
	)
}

export default List

const styles = StyleSheet.create({
	img:{
		width: width/4,
		height:height/8
	}
})
